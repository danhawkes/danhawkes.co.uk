from django.db import models
from datetime import datetime
import markdown
from articles.utils import ReadOnlyCachedAttribute


class Project(models.Model):
    _MARKUP_NONE = 'none'
    _MARKUP_MARKDOWN = 'markdown'
    _MARKUP_CHOICES = ((_MARKUP_NONE, 'None'), (_MARKUP_MARKDOWN, 'Markdown'))
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    summary = models.CharField(max_length=200, blank=True, help_text='A one-line summary of the project.')
    description = models.TextField(max_length=2000, blank=True)
    markup = models.CharField(max_length=20, choices=_MARKUP_CHOICES, default=_MARKUP_MARKDOWN, help_text='The markup language used in the description text.')
    icon = models.ImageField(blank=True, upload_to='img');
    date_published = models.DateField(verbose_name='publication date', default=datetime.now)
    licence = models.ForeignKey('Licence', null=True, blank=True)
    client = models.ForeignKey('Client')
    tags = models.ManyToManyField('Tag', null=True)
    rank = models.PositiveSmallIntegerField()
    
    
    def __init__(self, *args, **kwargs):
        models.Model.__init__(self, *args, **kwargs)
        self._rendered_description = markdown.markdown(self.description)

    def __unicode__(self):
        return self.name

    @ReadOnlyCachedAttribute
    def rendered_description(self):
        if self.markup == Project._MARKUP_MARKDOWN:
            return markdown.markdown(self.description)
        else:
            return self.description
        
    def links(self):
        links = []
        links.extend(self.filelink_set.all())
        links.extend(self.externallink_set.all())
        return links
    
    class Meta:
        ordering = ('rank',)
    
class Tag(models.Model):
    name = models.CharField(max_length=30, unique=True)

    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ('name',)
    
class Client(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Licence(models.Model):
    LICENCE_CHOICES = (('freebsd', 'FreeBSD'), ('apache', 'Apache'), ('gpl', 'GPL'), ('proprietary', 'Proprietary'))
    name = models.CharField(max_length=50, choices=LICENCE_CHOICES)
    version = models.CharField(max_length=20, blank=True)

    def __unicode__(self):
        return self.name + ' ' + self.version

class Link(models.Model):
    project = models.ForeignKey('Project')
    annotation = models.CharField(max_length=200, blank=True)
    target_blank = models.BooleanField(default=False, help_text='Forces the link to open in a new tab/window.')

    class Meta:
        abstract = True

class FileLink(Link):
    upload_to = lambda inst, filename: 'portfolio/%s/files/%s' % (inst.project.slug, filename)
    file = models.FileField(upload_to=upload_to)

class ExternalLink(Link):
    url = models.URLField()

class Image(models.Model):
    project = models.ForeignKey('Project')
    upload_to = lambda inst, filename: 'portfolio/%s/img/%s' % (inst.project.slug, filename)
    file = models.ImageField(upload_to=upload_to, width_field='width', height_field='height')
    caption = models.CharField(max_length=200, blank=True)
    width = models.PositiveIntegerField(editable=False)
    height = models.PositiveIntegerField(editable=False)
