from django.contrib import admin
from portfolio.models import Project, Licence, Image, FileLink, ExternalLink, Tag, Client




class ImageInline(admin.TabularInline):
    model = Image
    extra = 1

class FileLinkInline(admin.TabularInline):
    fieldsets = [(None, {'fields':['file', 'annotation', 'target_blank']})]
    model = FileLink
    extra = 1

class ExternalLinkInline(admin.TabularInline):
    fieldsets = [(None, {'fields':['url', 'annotation', 'target_blank']})]
    model = ExternalLink
    extra = 1

class ProjectAdmin(admin.ModelAdmin):

    fieldsets = [
        ('General', {'fields': ['name', 'slug', 'icon', 'summary']}),
        ('Description', {'fields': ['description', 'markup']}),
        ('Meta', {'fields': ['date_published', 'licence', 'tags', 'client', 'rank']}),
    ]
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'rank', 'summary')
    list_filter = ["date_published", 'client', 'tags', 'licence']
    search_fields = ["name"]
    inlines = [ImageInline, FileLinkInline, ExternalLinkInline]

admin.site.register(Tag)
admin.site.register(Licence)
admin.site.register(Client)
admin.site.register(Project, ProjectAdmin)
