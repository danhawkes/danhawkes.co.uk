from django.conf.urls import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from portfolio import views


urlpatterns = patterns('',
    url(r'^$', views.index, name='portfolio-index'),
    url(r'^processingjs/tentacles/$', views.tentacles, name='tentacles'),
    url(r'^(?P<project_id>\S+)/$', views.detail, name='portfolio-detail'),
)

urlpatterns += staticfiles_urlpatterns()
