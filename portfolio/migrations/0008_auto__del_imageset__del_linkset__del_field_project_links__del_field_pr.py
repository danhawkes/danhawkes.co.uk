# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'ImageSet'
        db.delete_table('portfolio_imageset')

        # Deleting model 'LinkSet'
        db.delete_table('portfolio_linkset')

        # Deleting field 'Project.links'
        db.delete_column('portfolio_project', 'links_id')

        # Deleting field 'Project.images'
        db.delete_column('portfolio_project', 'images_id')


        # Changing field 'ExternalLink.linkset'
        db.alter_column('portfolio_externallink', 'linkset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project']))

        # Changing field 'LocalLink.linkset'
        db.alter_column('portfolio_locallink', 'linkset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project']))

        # Changing field 'Image.imageset'
        db.alter_column('portfolio_image', 'imageset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project']))

    def backwards(self, orm):
        # Adding model 'ImageSet'
        db.create_table('portfolio_imageset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['ImageSet'])

        # Adding model 'LinkSet'
        db.create_table('portfolio_linkset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['LinkSet'])

        # Adding field 'Project.links'
        db.add_column('portfolio_project', 'links',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkSet'], null=True),
                      keep_default=False)

        # Adding field 'Project.images'
        db.add_column('portfolio_project', 'images',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSet'], null=True),
                      keep_default=False)


        # Changing field 'ExternalLink.linkset'
        db.alter_column('portfolio_externallink', 'linkset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkSet']))

        # Changing field 'LocalLink.linkset'
        db.alter_column('portfolio_locallink', 'linkset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkSet']))

        # Changing field 'Image.imageset'
        db.alter_column('portfolio_image', 'imageset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSet']))

    models = {
        'portfolio.externallink': {
            'Meta': {'object_name': 'ExternalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'portfolio.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'portfolio.image': {
            'Meta': {'object_name': 'Image'},
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imageset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']"})
        },
        'portfolio.licence': {
            'Meta': {'object_name': 'Licence'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'portfolio.locallink': {
            'Meta': {'object_name': 'LocalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portfolio.project': {
            'Meta': {'object_name': 'Project'},
            'date_published': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Group']"}),
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Licence']", 'null': 'True', 'blank': 'True'}),
            'markup': ('django.db.models.fields.CharField', [], {'default': "'markdown'", 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['portfolio.Tag']", 'null': 'True', 'symmetrical': 'False'})
        },
        'portfolio.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['portfolio']