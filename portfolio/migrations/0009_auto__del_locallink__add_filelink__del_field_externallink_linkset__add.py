# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'LocalLink'
        db.delete_table('portfolio_locallink')

        # Adding model 'FileLink'
        db.create_table('portfolio_filelink', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'])),
            ('annotation', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('target_blank', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('portfolio', ['FileLink'])

        # Deleting field 'ExternalLink.linkset'
        db.delete_column('portfolio_externallink', 'linkset_id')

        # Adding field 'ExternalLink.project'
        db.add_column('portfolio_externallink', 'project',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['portfolio.Project']),
                      keep_default=False)

        # Deleting field 'Image.imageset'
        db.delete_column('portfolio_image', 'imageset_id')


    def backwards(self, orm):
        # Adding model 'LocalLink'
        db.create_table('portfolio_locallink', (
            ('target_blank', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('linkset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'])),
            ('annotation', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('portfolio', ['LocalLink'])

        # Deleting model 'FileLink'
        db.delete_table('portfolio_filelink')


        # User chose to not deal with backwards NULL issues for 'ExternalLink.linkset'
        raise RuntimeError("Cannot reverse this migration. 'ExternalLink.linkset' and its values cannot be restored.")
        # Deleting field 'ExternalLink.project'
        db.delete_column('portfolio_externallink', 'project_id')


        # User chose to not deal with backwards NULL issues for 'Image.imageset'
        raise RuntimeError("Cannot reverse this migration. 'Image.imageset' and its values cannot be restored.")

    models = {
        'portfolio.externallink': {
            'Meta': {'object_name': 'ExternalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'portfolio.filelink': {
            'Meta': {'object_name': 'FileLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portfolio.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'portfolio.image': {
            'Meta': {'object_name': 'Image'},
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portfolio.licence': {
            'Meta': {'object_name': 'Licence'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'portfolio.project': {
            'Meta': {'object_name': 'Project'},
            'date_published': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Group']"}),
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Licence']", 'null': 'True', 'blank': 'True'}),
            'markup': ('django.db.models.fields.CharField', [], {'default': "'markdown'", 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['portfolio.Tag']", 'null': 'True', 'symmetrical': 'False'})
        },
        'portfolio.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['portfolio']