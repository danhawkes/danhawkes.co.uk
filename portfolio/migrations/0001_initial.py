# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Project'
        db.create_table('portfolio_project', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('description_short', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('description_long', self.gf('django.db.models.fields.TextField')(max_length=2000, blank=True)),
            ('markup_type', self.gf('django.db.models.fields.CharField')(default='m', max_length=20)),
            ('source_link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('icon', self.gf('django.db.models.fields.files.FileField')(max_length=100, blank=True)),
            ('date_published', self.gf('django.db.models.fields.DateTimeField')()),
            ('licence', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Licence'], null=True, blank=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Group'])),
        ))
        db.send_create_signal('portfolio', ['Project'])

        # Adding M2M table for field tags on 'Project'
        db.create_table('portfolio_project_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm['portfolio.project'], null=False)),
            ('tag', models.ForeignKey(orm['portfolio.tag'], null=False))
        ))
        db.create_unique('portfolio_project_tags', ['project_id', 'tag_id'])

        # Adding model 'Tag'
        db.create_table('portfolio_tag', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal('portfolio', ['Tag'])

        # Adding model 'Group'
        db.create_table('portfolio_group', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('portfolio', ['Group'])

        # Adding model 'Licence'
        db.create_table('portfolio_licence', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('portfolio', ['Licence'])

        # Adding model 'HtmlContentSection'
        db.create_table('portfolio_htmlcontentsection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('html', self.gf('django.db.models.fields.TextField')(max_length=2000, blank=True)),
        ))
        db.send_create_signal('portfolio', ['HtmlContentSection'])

        # Adding model 'FileDependency'
        db.create_table('portfolio_filedependency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('htmlcontentsection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.HtmlContentSection'])),
        ))
        db.send_create_signal('portfolio', ['FileDependency'])

        # Adding model 'LinkContentSection'
        db.create_table('portfolio_linkcontentsection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('portfolio', ['LinkContentSection'])

        # Adding model 'LocalLink'
        db.create_table('portfolio_locallink', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('linkcontentsection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkContentSection'])),
            ('annotation', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('target_blank', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('portfolio', ['LocalLink'])

        # Adding model 'ExternalLink'
        db.create_table('portfolio_externallink', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('linkcontentsection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkContentSection'])),
            ('annotation', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('target_blank', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal('portfolio', ['ExternalLink'])

        # Adding model 'ImageSetContentSection'
        db.create_table('portfolio_imagesetcontentsection', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('portfolio', ['ImageSetContentSection'])

        # Adding model 'Image'
        db.create_table('portfolio_image', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('caption', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('imageset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSetContentSection'])),
        ))
        db.send_create_signal('portfolio', ['Image'])


    def backwards(self, orm):
        # Deleting model 'Project'
        db.delete_table('portfolio_project')

        # Removing M2M table for field tags on 'Project'
        db.delete_table('portfolio_project_tags')

        # Deleting model 'Tag'
        db.delete_table('portfolio_tag')

        # Deleting model 'Group'
        db.delete_table('portfolio_group')

        # Deleting model 'Licence'
        db.delete_table('portfolio_licence')

        # Deleting model 'HtmlContentSection'
        db.delete_table('portfolio_htmlcontentsection')

        # Deleting model 'FileDependency'
        db.delete_table('portfolio_filedependency')

        # Deleting model 'LinkContentSection'
        db.delete_table('portfolio_linkcontentsection')

        # Deleting model 'LocalLink'
        db.delete_table('portfolio_locallink')

        # Deleting model 'ExternalLink'
        db.delete_table('portfolio_externallink')

        # Deleting model 'ImageSetContentSection'
        db.delete_table('portfolio_imagesetcontentsection')

        # Deleting model 'Image'
        db.delete_table('portfolio_image')


    models = {
        'portfolio.externallink': {
            'Meta': {'object_name': 'ExternalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkContentSection']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'portfolio.filedependency': {
            'Meta': {'object_name': 'FileDependency'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'htmlcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.HtmlContentSection']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portfolio.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'portfolio.htmlcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'HtmlContentSection'},
            'html': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.image': {
            'Meta': {'object_name': 'Image'},
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imageset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.ImageSetContentSection']"})
        },
        'portfolio.imagesetcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'ImageSetContentSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.licence': {
            'Meta': {'object_name': 'Licence'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'portfolio.linkcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'LinkContentSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.locallink': {
            'Meta': {'object_name': 'LocalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkContentSection']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portfolio.project': {
            'Meta': {'object_name': 'Project'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {}),
            'description_long': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'description_short': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Group']"}),
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Licence']", 'null': 'True', 'blank': 'True'}),
            'markup_type': ('django.db.models.fields.CharField', [], {'default': "'m'", 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'source_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['portfolio.Tag']", 'null': 'True', 'symmetrical': 'False'})
        },
        'portfolio.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['portfolio']