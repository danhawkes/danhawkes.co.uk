# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Project.description_short'
        db.delete_column('portfolio_project', 'description_short')

        # Adding field 'Project.summary'
        db.add_column('portfolio_project', 'summary',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Project.description_short'
        db.add_column('portfolio_project', 'description_short',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'Project.summary'
        db.delete_column('portfolio_project', 'summary')


    models = {
        'portfolio.externallink': {
            'Meta': {'object_name': 'ExternalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkContentSection']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'portfolio.filedependency': {
            'Meta': {'object_name': 'FileDependency'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'htmlcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.HtmlContentSection']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portfolio.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'portfolio.htmlcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'HtmlContentSection'},
            'html': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.image': {
            'Meta': {'object_name': 'Image'},
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imageset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.ImageSetContentSection']"})
        },
        'portfolio.imagesetcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'ImageSetContentSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.licence': {
            'Meta': {'object_name': 'Licence'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'portfolio.linkcontentsection': {
            'Meta': {'ordering': "['project', 'rank']", 'object_name': 'LinkContentSection'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Project']", 'null': 'True'}),
            'rank': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        },
        'portfolio.locallink': {
            'Meta': {'object_name': 'LocalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkcontentsection': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkContentSection']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portfolio.project': {
            'Meta': {'object_name': 'Project'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {}),
            'description_long': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Group']"}),
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Licence']", 'null': 'True', 'blank': 'True'}),
            'markup': ('django.db.models.fields.CharField', [], {'default': "'markdown'", 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'source_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['portfolio.Tag']", 'null': 'True', 'symmetrical': 'False'})
        },
        'portfolio.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['portfolio']