# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'LinkContentSection'
        db.delete_table('portfolio_linkcontentsection')

        # Deleting model 'HtmlContentSection'
        db.delete_table('portfolio_htmlcontentsection')

        # Deleting model 'ImageSetContentSection'
        db.delete_table('portfolio_imagesetcontentsection')

        # Deleting model 'FileDependency'
        db.delete_table('portfolio_filedependency')

        # Adding model 'ImageSet'
        db.create_table('portfolio_imageset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['ImageSet'])

        # Adding model 'LinkSet'
        db.create_table('portfolio_linkset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['LinkSet'])

        # Deleting field 'Project.source_link'
        db.delete_column('portfolio_project', 'source_link')

        # Adding field 'Project.links'
        db.add_column('portfolio_project', 'links',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.LinkSet'], null=True),
                      keep_default=False)

        # Adding field 'Project.images'
        db.add_column('portfolio_project', 'images',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSet'], null=True),
                      keep_default=False)

        # Deleting field 'ExternalLink.linkcontentsection'
        db.delete_column('portfolio_externallink', 'linkcontentsection_id')

        # Adding field 'ExternalLink.linkset'
        db.add_column('portfolio_externallink', 'linkset',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['portfolio.LinkSet']),
                      keep_default=False)

        # Deleting field 'LocalLink.linkcontentsection'
        db.delete_column('portfolio_locallink', 'linkcontentsection_id')

        # Adding field 'LocalLink.linkset'
        db.add_column('portfolio_locallink', 'linkset',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['portfolio.LinkSet']),
                      keep_default=False)


        # Changing field 'Image.imageset'
        db.alter_column('portfolio_image', 'imageset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSet']))

    def backwards(self, orm):
        # Adding model 'LinkContentSection'
        db.create_table('portfolio_linkcontentsection', (
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('portfolio', ['LinkContentSection'])

        # Adding model 'HtmlContentSection'
        db.create_table('portfolio_htmlcontentsection', (
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('html', self.gf('django.db.models.fields.TextField')(max_length=2000, blank=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['HtmlContentSection'])

        # Adding model 'ImageSetContentSection'
        db.create_table('portfolio_imagesetcontentsection', (
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.Project'], null=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rank', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('portfolio', ['ImageSetContentSection'])

        # Adding model 'FileDependency'
        db.create_table('portfolio_filedependency', (
            ('htmlcontentsection', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.HtmlContentSection'])),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('portfolio', ['FileDependency'])

        # Deleting model 'ImageSet'
        db.delete_table('portfolio_imageset')

        # Deleting model 'LinkSet'
        db.delete_table('portfolio_linkset')

        # Adding field 'Project.source_link'
        db.add_column('portfolio_project', 'source_link',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'Project.links'
        db.delete_column('portfolio_project', 'links_id')

        # Deleting field 'Project.images'
        db.delete_column('portfolio_project', 'images_id')


        # User chose to not deal with backwards NULL issues for 'ExternalLink.linkcontentsection'
        raise RuntimeError("Cannot reverse this migration. 'ExternalLink.linkcontentsection' and its values cannot be restored.")
        # Deleting field 'ExternalLink.linkset'
        db.delete_column('portfolio_externallink', 'linkset_id')


        # User chose to not deal with backwards NULL issues for 'LocalLink.linkcontentsection'
        raise RuntimeError("Cannot reverse this migration. 'LocalLink.linkcontentsection' and its values cannot be restored.")
        # Deleting field 'LocalLink.linkset'
        db.delete_column('portfolio_locallink', 'linkset_id')


        # Changing field 'Image.imageset'
        db.alter_column('portfolio_image', 'imageset_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portfolio.ImageSetContentSection']))

    models = {
        'portfolio.externallink': {
            'Meta': {'object_name': 'ExternalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkSet']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'portfolio.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'portfolio.image': {
            'Meta': {'object_name': 'Image'},
            'caption': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imageset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.ImageSet']"})
        },
        'portfolio.imageset': {
            'Meta': {'object_name': 'ImageSet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portfolio.licence': {
            'Meta': {'object_name': 'Licence'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'portfolio.linkset': {
            'Meta': {'object_name': 'LinkSet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'portfolio.locallink': {
            'Meta': {'object_name': 'LocalLink'},
            'annotation': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linkset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkSet']"}),
            'target_blank': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'portfolio.project': {
            'Meta': {'object_name': 'Project'},
            'date_published': ('django.db.models.fields.DateField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '2000', 'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Group']"}),
            'icon': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.ImageSet']", 'null': 'True'}),
            'licence': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.Licence']", 'null': 'True', 'blank': 'True'}),
            'links': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portfolio.LinkSet']", 'null': 'True'}),
            'markup': ('django.db.models.fields.CharField', [], {'default': "'markdown'", 'max_length': '20'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['portfolio.Tag']", 'null': 'True', 'symmetrical': 'False'})
        },
        'portfolio.tag': {
            'Meta': {'object_name': 'Tag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['portfolio']