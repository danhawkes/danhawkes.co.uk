from django.shortcuts import render_to_response
from portfolio.models import Project, Tag, Client
from django.template.context import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    project_list = Project.objects.all().order_by('rank')
    param_dict = {'page_title':'Portfolio', 'project_list':project_list}
    return render_to_response('portfolio/index.html', param_dict, RequestContext(request))

@login_required
def detail(request, project_id):
    try:
        project = Project.objects.get(pk=int(project_id, 10))
    except ObjectDoesNotExist:
        raise Http404
    except ValueError:
        try:
            project = Project.objects.get(slug__iexact=project_id)
        except ObjectDoesNotExist:
            raise Http404
    
    if request.GET.get('embedded', False) == 'true':
        template = 'portfolio/embedded.html'
        param_dict = {'page_title':'Portfolio', 'project':project, 'title':'Portfolio', 'subtitle':project.name}
    else:
        template = 'portfolio/detail.html'
        param_dict = {'page_title':'Portfolio', 'project':project, 'title':'Portfolio', 'subtitle':project.name}
    
    return render_to_response(template, param_dict, RequestContext(request))

@login_required
def tentacles(request):
    return render_to_response('portfolio/processing/tentacles.html')
    
