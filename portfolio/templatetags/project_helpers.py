from django import template

register = template.Library()

@register.filter()
def groupfilter(projects, group):
    return filter(lambda project: project.group == group, projects)
        
@register.inclusion_tag('portfolio/components/tag.html')
def render_tag(tag):
    return {'tag':tag}

@register.inclusion_tag('portfolio/components/tags.html')
def render_tags(tags):
    return {'tags':tags}

@register.inclusion_tag('portfolio/components/link.html')
def render_link(link):
    return {'link':link}

@register.inclusion_tag('portfolio/components/link_list.html')
def render_link_list(links):
    return {'links':links}

@register.inclusion_tag('portfolio/components/orbitimageviewer.html')
def render_orbit_image_viewer(images, tag_id='', tag_class=''):
    images = list(images)
    if images:
        max_height = max(images, key=lambda image: image.height).height
        return {'images':images, 'tag_id':tag_id, 'tag_class':tag_class, 'max_height':max_height}
    else:
        return ''
