from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from danhawkes import settings, views
from django.views.generic.simple import direct_to_template


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', views.index, name='danhawkes-home'),
    url(r'^portfolio/', include('portfolio.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include('articles.urls')),
    url(r'^cv/', views.cv, name='danhawkes-cv'),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', \
        {'template_name': 'danhawkes/login.html'}, name='danhawkes-login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', \
        {'template_name': 'danhawkes/logout.html'}, name='danhawkes-logout'),
    url(r'^accounts/profile/$', direct_to_template, \
        {'template': 'danhawkes/profile.html'}, name='danhawkes-profile'),
    url(r'^robots.txt$', direct_to_template, \
        {'template': 'danhawkes/robots.txt', 'mimetype': 'text/plain'}),
    url(r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', \
        {'url': settings.STATIC_URL + 'danhawkes/img/favicon.ico'}),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),)

urlpatterns += staticfiles_urlpatterns()
