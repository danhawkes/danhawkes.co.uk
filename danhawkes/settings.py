# -*- coding: utf-8 -*-
# Django settings for danhawkes project.

import os.path
from os import path

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    (u'Daniel Hawkes', 'admin@danhawkes.co.uk'),
)

MANAGERS = ADMINS


# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

###

#BASE_PATH = '/Users/danhawkes/Documents/homeworkspace/danhawkes.co.uk/'
BASE_PATH = os.path.dirname(__file__)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = BASE_PATH + MEDIA_URL


# URL to use when referring to static files located in STATIC_ROOT.
# Examples: "/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# The absolute path to the directory where collectstatic will collect static files for deployment.
# Example: "/home/example.com/static/"
STATIC_ROOT = BASE_PATH + STATIC_URL

DEFAULT_FILE_STORAGE = 'danhawkes.storage.CustomStorage'


###

# Make this unique, and don't share it with anybody.
SECRET_KEY = ')(toq^spax$b6$(vm-axipy)wjcw$q588#rz0%@wnxipncdnxp'

ROOT_URLCONF = 'danhawkes.urls'

###

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    BASE_PATH + '/templates/',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.gzip.GZipMiddleware',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.humanize',
    'django.contrib.markup',
    'comments',
    'sitetree',
    'south',
    'danhawkes',
    'portfolio',
    'tagging',
    'articles',
    'django_dispose',
)

DISPOSE_MEDIA = {
    'portfolio': {
        'root': path.join(MEDIA_ROOT, 'portfolio'),
        'skip': [path.join(MEDIA_ROOT, 'portfolio', 'processingjs')]
    },
    'danhawkes': {
        'root': path.join(MEDIA_ROOT, 'danhawkes')
    },
}

LOGIN_REDIRECT_URL = '/'
