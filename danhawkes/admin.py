from django.contrib import admin
from danhawkes.models import SiteInfo, CurriculumVitae


class SiteInfoAdmin(admin.ModelAdmin):

    fieldsets = [
        ('General', {'fields': ['display_name', 'contact_email' ]}),
    ]
#    prepopulated_fields = {'slug': ('name',)}
#    list_display = ("name", 'description_short', 'content_sections')
#    list_filter = ["date_published"]
#    search_fields = ["name"]

admin.site.register(SiteInfo, SiteInfoAdmin)
admin.site.register(CurriculumVitae)

