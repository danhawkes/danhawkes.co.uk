from settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'danhawkes.wsgi.application'

# Make this unique, and don't share it with anybody.
SECRET_KEY = ')(toq^spax$b6$(vm-axipy)wjcw$q588#rz0%@wnxipncdnxp'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'danhawkes_danhawkes',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/London'
