from django.db import models


class SiteInfo(models.Model):

    display_name = models.CharField(max_length=50)
    contact_email = models.EmailField()

    def __unicode__(self):
        return self.display_name

    class Meta:
        verbose_name = 'site'


class CurriculumVitae(models.Model):
    content = models.TextField(blank=True, max_length=5000)
    upload_to = lambda inst, filename: 'danhawkes/cv/files/%s' % (filename)
    file = models.FileField(upload_to=upload_to)
    contact_email = models.EmailField(max_length=50, blank=True)
    contact_email_display = models.CharField(max_length=50, blank=True)
    contact_telephone = models.CharField(max_length=20, blank=True)
    contact_telephone_display = models.CharField(max_length=20, blank=True)
    contact_skype = models.CharField(max_length=50, blank=True)
    contact_skype_display = models.CharField(max_length=50, blank=True)
