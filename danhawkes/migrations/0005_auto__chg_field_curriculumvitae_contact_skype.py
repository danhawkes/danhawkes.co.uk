# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'CurriculumVitae.contact_skype'
        db.alter_column('danhawkes_curriculumvitae', 'contact_skype', self.gf('django.db.models.fields.CharField')(max_length=50))

    def backwards(self, orm):

        # Changing field 'CurriculumVitae.contact_skype'
        db.alter_column('danhawkes_curriculumvitae', 'contact_skype', self.gf('django.db.models.fields.URLField')(max_length=50))

    models = {
        'danhawkes.curriculumvitae': {
            'Meta': {'object_name': 'CurriculumVitae'},
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_email_display': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_skype': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_skype_display': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_telephone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'contact_telephone_display': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'max_length': '5000', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'danhawkes.siteinfo': {
            'Meta': {'object_name': 'SiteInfo'},
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['danhawkes']