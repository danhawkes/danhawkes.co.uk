# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CurriculumVitae.contact_code'
        db.add_column('danhawkes_curriculumvitae', 'contact_code',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=50, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'CurriculumVitae.contact_code'
        db.delete_column('danhawkes_curriculumvitae', 'contact_code')


    models = {
        'danhawkes.curriculumvitae': {
            'Meta': {'object_name': 'CurriculumVitae'},
            'contact_code': ('django.db.models.fields.URLField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_im': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_telephone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'max_length': '5000', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'danhawkes.siteinfo': {
            'Meta': {'object_name': 'SiteInfo'},
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['danhawkes']