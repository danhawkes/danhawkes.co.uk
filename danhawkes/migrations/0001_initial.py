# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SiteInfo'
        db.create_table('danhawkes_siteinfo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('display_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal('danhawkes', ['SiteInfo'])

        # Adding model 'CurriculumVitae'
        db.create_table('danhawkes_curriculumvitae', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=5000, blank=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('contact_email', self.gf('django.db.models.fields.EmailField')(max_length=50, blank=True)),
            ('contact_telephone', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('contact_im', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('danhawkes', ['CurriculumVitae'])


    def backwards(self, orm):
        # Deleting model 'SiteInfo'
        db.delete_table('danhawkes_siteinfo')

        # Deleting model 'CurriculumVitae'
        db.delete_table('danhawkes_curriculumvitae')


    models = {
        'danhawkes.curriculumvitae': {
            'Meta': {'object_name': 'CurriculumVitae'},
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_im': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'contact_telephone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'max_length': '5000', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'danhawkes.siteinfo': {
            'Meta': {'object_name': 'SiteInfo'},
            'contact_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['danhawkes']