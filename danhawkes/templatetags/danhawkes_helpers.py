from django import template

register = template.Library()

@register.tag
def site_display_name(parser, token):
    tag_name = token.split_contents()
    if len(tag_name) > 1:
        raise template.TemplateSyntaxError("%r tag expects no arguments" % tag_name[0])
    return SiteDisplayNameContentNode('danhawkes.co.uk')

class SiteDisplayNameContentNode(template.Node):

    def __init__(self, site_display_name):
        self.site_display_name = site_display_name

    def render(self, context):
            return self.site_display_name

@register.tag
def contact_email(parser, token):
    tag_name = token.split_contents()
    if len(tag_name) > 1:
        raise template.TemplateSyntaxError("%r tag expects no arguments" % tag_name[0])
    return ContactEmailContentNode('contact@danhawkes.co.uk', 'Contact')

class ContactEmailContentNode(template.Node):

    def __init__(self, email, label):
        self.email = email
        self.label = label

    def render(self, context):
            return '<a href="mailto:' + self.email + '">' + self.label + '</a>'
