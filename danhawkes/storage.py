from django.core.files.storage import FileSystemStorage
import os
import random
import string

class CustomStorage(FileSystemStorage):

    def get_available_name(self, name):
        filename = ''
        while True:
            filename = gen_name(name)
            if not self.exists(filename):
                break
        return filename

def gen_name(name):
    dir_name, file_name = os.path.split(name)
    _file_root, file_ext = os.path.splitext(file_name)
    return os.path.join(dir_name, "%s%s" % (random_id(), file_ext))

def random_id():
    return ''.join(random.choice(string.lowercase) for _x in range(16))
