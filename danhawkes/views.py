from django.shortcuts import redirect, render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from danhawkes.models import CurriculumVitae
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.core.mail import send_mail
from datetime import datetime


def index(request):
    token = request.GET.get('t', '')
    if token:
        # Handle automatic login tokens
        user = authenticate(username=token, password=token)
        if user and user.is_active:
            login(request, user)
            return redirect(reverse('portfolio-index'))
        else:
            msg = 'An autologin attempt failed at %s.\n\n%s' % \
            ((datetime.now()), request.GET)
            send_mail('Failed autologin', msg, 'admin@danhawkes.co.uk', \
                      ('dan@danhawkes.co.uk',), True)
            return render_to_response('danhawkes/autologin.html', \
                                  {'token': token}, RequestContext(request))
    else:
        # Regular users just go to the blog
        return redirect(reverse('articles_archive'))


@login_required
def cv(request):
    cv = CurriculumVitae.objects.get()
    param_dict = {'page_title': 'CV', 'model': cv}
    return render_to_response('danhawkes/cv.html', param_dict, \
                              RequestContext(request))
