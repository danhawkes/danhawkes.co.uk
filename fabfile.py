from fabric.api import * #@UnusedWildImport
from danhawkes import settings_deploy, settings_dev
import os.path
from fabric.colors import blue, yellow, green

env.hosts = ['danhawkes@danhawkes.co.uk']

django_root = 'danhawkes'
repo = 'git@bitbucket.org:danhawkes/danhawkes.co.uk.git'
cloned_repo = 'danhawkes'

code_repo = 'git@bitbucket.org:danhawkes/bits.git'
cloned_code_repo = 'danhawkes/articles/static/bits'

db_dump = 'db_dump.sql'

backup = 'backup'
backup_temp = os.path.join(backup, 'temp')

'''
Note: 'settings_deploy.py' referred to in this file is absent from the public repo as it contains private DB passwords.
'''

@task
@runs_once
def backup():
    '''Creates a backup on the remote machine.'''
    print(blue('Creating backup...'))
    with cd('~'):
        if os.path.isdir(django_root):
            run('mkdir -p ' + backup_temp)
            # Dump db to temp dir
            db = settings_deploy.DATABASES['default']
            run('mysqldump -u' + db['USER'] + ' -p' + db['PASSWORD'] + ' --add-drop-table ' + db['NAME'] + ' > ' + os.path.join(backup_temp, db_dump))
            # Copy files to temp dir
            run('cp -R ' + django_root + ' ' + os.path.join(backup_temp, django_root))
            # Zip it all and delete temp dir
            run('tar -czf "backup/$(date -u \'+%d-%m-%Y %H-%M-%S %Z\').tar.gz" ' + backup_temp)
            run('rm -rf ' + backup_temp)

@task
@runs_once
def delete():
    '''Deletes all project files on the remote machine.'''
    print(blue('Deleting old project files...'))
    with cd('~'):
        run('rm -rf ' + django_root + ' 2> /dev/null')

@task
@runs_once
def clone_repo():
    '''Creates a clone of the repo on the remote machine.'''
    delete()
    print(blue('Cloning repo from git...'))
    with cd('~'):
        run('rm -rf ' + cloned_repo + ' 2> /dev/null');
        run('git clone ' + repo + ' ' + cloned_repo)
        run('cp settings_deploy.py ' + os.path.join(django_root, django_root))

@task
@runs_once
def clone_code_repo():
    '''Creates a clone of the code snippets repo on the remote machine.'''
    print(blue('Cloning code repo from git...'))
    with cd('~'):
        run('rm -rf ' + cloned_code_repo + ' 2> /dev/null');
        run('git clone ' + code_repo + ' ' + cloned_code_repo)

@task
@runs_once
def update_media():
    '''Uploads local media files to the remote machine.'''
    print(blue('Uploading media files to remote machine...'))
    with lcd(django_root):
        put('media', os.path.join(django_root, django_root))

@task
@runs_once
def collect_static():
    '''Collects static files on the remote machine.'''
    print(blue('Collecting static files...'))
    with cd('~'):
        run(os.path.join(django_root, 'manage.py') + ' collectstatic --noinput')

@runs_once
def create_local_db_dump():
    '''Creates/updates the local DB dump.'''
    script_dir = os.path.dirname(os.path.realpath(__file__))
    with lcd(script_dir):
        print(blue('Creating dump of local DB...'))
        local('mysqldump -u root --add-drop-table ' + settings_dev.DATABASES['default']['NAME'] + ' > ' + db_dump)
        
@runs_once
def delete_local_db_dump():
    script_dir = os.path.dirname(os.path.realpath(__file__))
    with lcd(script_dir):
        local('rm ' + db_dump)

@runs_once
def upload_db_dump():
    '''Uploads the local db dump to the remote machine.'''
    create_local_db_dump()
    print(blue('Pushing DB dump to remote machine...'))
    run('rm -f ' + db_dump + ' 2> /dev/null')
    put(db_dump, '~/' + django_root + '/' + db_dump)
    delete_local_db_dump()

@task
@runs_once
def update_db():
    '''Recreates the remote DB from a local dump.'''
    upload_db_dump()
    # Note: this references the remote DB password in a file called 'settings_deploy.py'. It's not in the public repo!
    db = settings_deploy.DATABASES['default']
    print(blue('Upgrading remote DB...') + yellow(' (this may take a while!)'))
    with cd('~/' + django_root):
        run("mysql -u%s -p%s %s < %s" % (db['USER'], db['PASSWORD'], db['NAME'], db_dump))
        run('rm ' + db_dump)

@runs_once
def restart_django():
    '''Restarts the remote Django instance.'''
    print(blue('Restarting Django...'))
    run('~/init/danhawkes restart')

@task
def deploy():
    '''Backs up the current deployment, clones latest source from git, recreates DB and media files from local copy, restarts Django.'''
    backup()
    delete()
    clone_repo()
    clone_code_repo()
    update_media()
    collect_static()
    update_db()
    restart_django()
    print(green('Deployment complete!'))
